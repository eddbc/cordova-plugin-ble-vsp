/*******************************************************************************
 * Copyright (c) 2014 Laird Technologies. All Rights Reserved.
 *
 * The information contained herein is property of Laird Technologies.
 * Licensees are granted free, non-transferable use of the information. NO WARRANTY of ANY KIND is provided. 
 * This heading must NOT be removed from the file.
 *******************************************************************************/

package com.lairdtech.misc;

import android.util.Log;
import com.eddbc.ByteArrayBuilder;

/**
 * storing data into a buffer and makes it easy to read, write data to it. <br>
 * Used by the VSP functionalities to store the data that is receiving and the
 * data that will be sending
 */
public class FifoByteQueue
{
	private static final String TAG = "FifoQueue";

	private ByteArrayBuilder mBufferData = new ByteArrayBuilder();

	public int getSize()
	{
		return mBufferData.length();
	}

	/**
	 * clears the whole buffer
	 */
	public void flush()
	{
		mBufferData.delete(0, mBufferData.length());
	}

	/**
	 * Add new data in buffer, the data will be kept in the buffer and get
	 * merged with new data whenever this method is called
	 *
	 * @param value
	 *            the data to be appended in the buffer
	 */
	public void write(byte[] value)
	{
		mBufferData.append(value);
	}

	/**
	 * read all data from buffer, data read gets removed from the buffer
	 *
	 * @param dest
	 *            the object to store the data read
	 * @return the total number of characters read
	 */
	public int read(ByteArrayBuilder dest)
	{
		if (mBufferData.length() > 0)
		{
			// we have data in the buffer
			dest.append(mBufferData.section(0, mBufferData.length()));
			mBufferData.delete(0, mBufferData.length());

			return dest.length();
		}

		return 0;
	}

	/**
	 * read the maximum data which is defined by the mMaxDataToBeReadFromBuffer
	 * parameter. If there is less data than the mMaxDataToBeReadFromBuffer then
	 * all the data gets read from the buffer. The data read gets removed from
	 * the buffer.
	 *
	 * @param dest
	 *            the object to store the data read
	 * @param maxDataToBeReadFromBuffer
	 *            the maximum data to read from the buffer
	 * @return the total number of characters read
	 */
	public int read(ByteArrayBuilder dest, int maxDataToBeReadFromBuffer)
	{
		Log.i(TAG, "reading " + maxDataToBeReadFromBuffer
				+ " bytes from the FIFO queue");

		if (mBufferData.length() <= maxDataToBeReadFromBuffer)
		{
			/*
			 * ignore the mMaxDataToBeReadFromBuffer variable and return the
			 * last data remaining from the buffer
			 */
			dest.append(mBufferData.section(0, mBufferData.length()));
			mBufferData.delete(0, mBufferData.length());
			return dest.length();
		}
		else if (mBufferData.length() > maxDataToBeReadFromBuffer)
		{
			/*
			 * return data from the buffer until the mMaxDataToBeReadFromBuffer
			 */
			dest.append(mBufferData.section(0, maxDataToBeReadFromBuffer));
			mBufferData.delete(0, maxDataToBeReadFromBuffer);
			return dest.length();
		}

		return 0;
	}
}